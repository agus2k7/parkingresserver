const controller = {}

controller.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT cli.*, \
                    tv.tipo, ma.marca, mo.modelo \
                    FROM clientes cli \
                    INNER JOIN modelos mo ON cli.modelo_id = mo.id \
                    INNER JOIN marcas ma ON mo.marca_id = ma.id \
                    INNER JOIN tipo_vehiculos tv ON mo.tipo_id = tv.id \
                    WHERE cli.baja = 0', (err, clientes) => {
            if (!err){
                res.json(clientes)
            } else {
                console.log(err)
            }
        })
    })
}

controller.save = (req, res) => {
    const data = req.body
    console.log(data)
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO clientes SET ?', [data], (err, clientes) => {
            if(!err){
                res.json({status: 'Cliente guardado'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.delete = (req, res) => {
    console.log(req.params) //parametros de url
    // const id = req.params.id
    const { id } = req.params //otra forma mas corta de escribir lo mismo
    req.getConnection((err,conn) => {
        conn.query('UPDATE clientes SET baja = 1 WHERE id = ?', [id], (err, rows) => {
            if(!err){
                res.json({status: 'Cliente eliminado'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.update = (req, res) => {
    const {id} = req.params
    const nuevoCliente = req.body
    req.getConnection( (req, conn) => {
        conn.query('UPDATE clientes SET ? WHERE id = ?', [nuevoCliente, id], (err, rows) => {
            if(!err){
                res.json({status: 'Cliente actualizado'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.edit = (req, res) => {
    const { id } = req.params
    req.getConnection( (req, conn) => {
        conn.query('SELECT * FROM clientes WHERE id = ?', [id], (err, clientes) => {
            if(!err){
                res.json(clientes[0])
            } else {
                console.log(err)
            }
        })
    })
}

controller.patente = (req, res) => {
    const { patente } = req.params
    console.log('hola')
    req.getConnection( (req, conn) => {
        conn.query('SELECT cli.*, mo.tipo_id  \
                    FROM clientes cli  \
                    INNER JOIN modelos mo ON cli.modelo_id = mo.id \
                    INNER JOIN tipo_vehiculos tv ON mo.tipo_id = tv.id \
                    INNER JOIN marcas ma ON mo.marca_id = ma.id \
                    WHERE patente = ? \
                    AND cli.baja = 0', [patente], (err, clientes) => {
            if(!err){
                res.json(clientes[0])
            } else {
                console.log(err)
            }
        })
    })
}

module.exports = controller