const controller = {}

controller.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM turnos WHERE baja = 0', (err, turnos) => {
            if (!err){
                res.json(turnos)
            } else {
                console.log(err)
            }
        })
    })
}

controller.save = (req, res) => {
    const data = req.body
    console.log(data)
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO turnos SET ?', [data], (err, turnos) => {
            if(!err){
                res.json({status: 'Turno guardado'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.delete = (req, res) => {
    console.log(req.params) //parametros de url
    // const id = req.params.id
    const { id } = req.params //otra forma mas corta de escribir lo mismo
    req.getConnection((err,conn) => {
        conn.query('UPDATE turnos SET baja = 1 WHERE id = ?', [id], (err, rows) => {
            if(!err){
                res.json({status: 'Turno eliminado'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.update = (req, res) => {
    const {id} = req.params
    const nuevoTurno = req.body
    req.getConnection( (req, conn) => {
        conn.query('UPDATE turnos SET ? WHERE id = ?', [nuevoTurno, id], (err, rows) => {
            if(!err){
                res.json({status: 'Turno actualizado'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.edit = (req, res) => {
    const { id } = req.params
    req.getConnection( (req, conn) => {
        conn.query('SELECT * FROM turnos WHERE id = ?', [id], (err, turnos) => {
            if(!err){
                res.json(turnos[0])
            } else {
                console.log(err)
            }
        })
    })
}

module.exports = controller