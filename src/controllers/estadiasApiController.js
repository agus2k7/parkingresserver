const controller = {}

controller.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT es.*, \
                    tu.turno, tv.tipo, ma.marca, mo.modelo, ta.precio, ta.tipo_id \
                    FROM estadias es \
                    INNER JOIN turnos tu ON es.turno_id = tu.id  \
                    INNER JOIN modelos mo ON es.modelo_id = mo.id \
                    INNER JOIN marcas ma ON mo.marca_id = ma.id \
                    INNER JOIN tarifas ta ON es.tarifa_id = ta.id \
                    INNER JOIN tipo_vehiculos tv ON ta.tipo_id = tv.id \
                    WHERE es.baja = 0', (err, estadias) => {
            if (!err){
                res.json(estadias)
            } else {
                console.log(err)
            }
        })
    })
}

controller.save = (req, res) => {
    const data = req.body
    console.log(data)
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO estadias SET ?', [data], (err, estadias) => {
            if(!err){
                res.json({status: 'Estadía guardada'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.delete = (req, res) => {
    console.log(req.params) //parametros de url
    // const id = req.params.id
    const { id } = req.params //otra forma mas corta de escribir lo mismo
    req.getConnection((err,conn) => {
        conn.query('UPDATE estadias SET baja = 1 WHERE id = ?', [id], (err, rows) => {
            if(!err){
                res.json({status: 'Estadía eliminada'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.update = (req, res) => {
    const {id} = req.params
    const nuevaEstadia = req.body
    req.getConnection( (req, conn) => {
        conn.query('UPDATE estadias SET ? WHERE id = ?', [nuevaEstadia, id], (err, rows) => {
            if(!err){
                res.json({status: 'Estadía actualizada'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.edit = (req, res) => {
    const { id } = req.params
    req.getConnection( (req, conn) => {
        conn.query('SELECT * FROM estadias WHERE id = ?', [id], (err, estadias) => {
            if(!err){
                res.json(estadias[0])
            } else {
                console.log(err)
            }
        })
    })
}

module.exports = controller