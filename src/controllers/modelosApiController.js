const controller = {}

controller.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT mo.id, mo.marca_id, mo.tipo_id, mo.modelo, mo.baja, ma.marca, tv.tipo \
                    FROM modelos mo \
                    INNER JOIN marcas ma ON mo.marca_id = ma.id \
                    INNER JOIN tipo_vehiculos tv ON mo.tipo_id = tv.id \
                    WHERE mo.baja = 0', (err, modelos) => {
            if (!err){
                res.json(modelos)
            } else {
                console.log(err)
            }
        })
    })
}

controller.save = (req, res) => {
    const data = req.body
    console.log(data)
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO modelos SET ?', [data], (err, modelos) => {
            if(!err){
                res.json({status: 'Modelo guardado'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.delete = (req, res) => {
    console.log(req.params) //parametros de url
    // const id = req.params.id
    const { id } = req.params //otra forma mas corta de escribir lo mismo
    req.getConnection((err,conn) => {
        conn.query('UPDATE modelos SET baja = 1 WHERE id = ?', [id], (err, rows) => {
            if(!err){
                res.json({status: 'Modelo eliminado'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.update = (req, res) => {
    const {id} = req.params
    const nuevoModelo = req.body
    req.getConnection( (req, conn) => {
        conn.query('UPDATE modelos SET ? WHERE id = ?', [nuevoModelo, id], (err, rows) => {
            if(!err){
                res.json({status: 'Modelo actualizado'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.edit = (req, res) => {
    const { id } = req.params
    req.getConnection( (req, conn) => {
        conn.query('SELECT * FROM modelos WHERE id = ?', [id], (err, modelos) => {
            if(!err){
                res.json(modelos[0])
            } else {
                console.log(err)
            }
        })
    })
}

module.exports = controller