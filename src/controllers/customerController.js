//voy a crear un objeto porque puedo tener muchos metodos
const controller = {}

//voy armando mis métodos (list, delete, save, etc) y exporto el objeto
controller.list = (req,res) => {
    // res.send('hello world')
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM CUSTOMER', (err, customers) => {
            //callback si da error
            if (err) {
                // next(err)
                res.json(err) //para dev conviene usar next en vez de res
            }
            // console.log(customers)
            //si no hay error renderizo una vista
            res.render('customers', {
                data: customers //le paso los customers a una variable data
            }) //no hace falta aclarar que es .ejs porque se configuro en app.js
        })
    })
}

controller.save = (req, res) => {
    const data = req.body //cuerpo http del formulario
    req.getConnection((err,conn) => {
        conn.query('INSERT INTO customer set ?', [data], (err, customer) => {
            // console.log(customer)
            // res.send('works')
            res.redirect('/')
        })
    })
}

controller.delete = (req, res) => {
    console.log(req.params) //parametros de url
    // const id = req.params.id
    const { id } = req.params //otra forma mas corta de escribir lo mismo
    console.log(id)
    req.getConnection((err,conn) => {
        conn.query('DELETE FROM customer WHERE id = ?', [id], (err, rows) => {
    //         console.log(customer)
    //         // res.send('works')
            res.redirect('/')
        })
    })
}

controller.edit = (req, res) => {
    const { id } = req.params
    req.getConnection( (req, conn) => {
        conn.query('SELECT * FROM CUSTOMER WHERE id = ?', [id], (err, customer) => {
            res.render('customer_edit', { //renderizo la vista de edición y le paso a data el customer
                data : customer[0]
            })
        })
    })

}

controller.update = (req, res) => {
    const {id} = req.params
    const newCustomer = req.body
    req.getConnection( (req, conn) => {
        conn.query('UPDATE customer SET ? WHERE id = ?', [newCustomer, id], (err, rows) => {
            res.redirect('/')
        })
    })
}


module.exports = controller