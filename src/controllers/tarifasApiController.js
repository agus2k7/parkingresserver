const controller = {}

controller.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT t.id, t.tipo_id, t.tiempo_id, t.precio, t.fecha, t.baja, tv.tipo \
                    FROM tarifas t \
                    INNER JOIN tipo_vehiculos tv ON t.tipo_id = tv.id \
                    INNER JOIN tiempo_tarifas tt ON t.tiempo_id = tt.id \
                    WHERE t.baja = 0', (err, tarifas) => {
            if (!err){
                res.json(tarifas)
            } else {
                console.log(err)
            }
        })
    })
}

controller.save = (req, res) => {
    const data = req.body
    console.log(data)
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO tarifas SET ?', [data], (err, tarifas) => {
            if(!err){
                res.json({status: 'Tarifa guardada'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.delete = (req, res) => {
    console.log(req.params) //parametros de url
    // const id = req.params.id
    const { id } = req.params //otra forma mas corta de escribir lo mismo
    req.getConnection((err,conn) => {
        conn.query('UPDATE tarifas SET baja = 1 WHERE id = ?', [id], (err, rows) => {
            if(!err){
                res.json({status: 'Tarifa eliminada'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.update = (req, res) => {
    const {id} = req.params
    const nuevaTarifa = req.body
    req.getConnection( (req, conn) => {
        conn.query('UPDATE tarifas SET ? WHERE id = ?', [nuevaTarifa, id], (err, rows) => {
            if(!err){
                res.json({status: 'Tarifa actualizada'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.edit = (req, res) => {
    const { id } = req.params
    req.getConnection( (req, conn) => {
        conn.query('SELECT * FROM tarifas WHERE id = ?', [id], (err, tarifas) => {
            if(!err){
                res.json(tarifas[0])
            } else {
                console.log(err)
            }
        })
    })
}

module.exports = controller