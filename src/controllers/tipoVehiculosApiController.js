//voy a crear un objeto porque puedo tener muchos metodos
const controller = {}

controller.list = (req, res) => {
    // res.send("Hola mundo")
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM tipo_vehiculos WHERE baja = 0', (err, tipoVehiculos) => {
            if (!err){
                res.json(tipoVehiculos)
            } else {
                // res.render('tipo_vehiculos', {
                //     data: tipoVehiculos})
                console.log(err)
            }
        })
    })
}

controller.save = (req, res) => {
    const data = req.body
    console.log(data)
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO tipo_vehiculos SET ?', [data], (err, tipoVehiculos) => {
            // console.log(tipoVehiculos)
            // res.redirect('/tipovehiculos')
            if(!err){
                res.json({status: 'Tipo de vehiculo guardado'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.delete = (req, res) => {
    console.log(req.params) //parametros de url
    // const id = req.params.id
    const { id } = req.params //otra forma mas corta de escribir lo mismo
    console.log(id)
    req.getConnection((err,conn) => {
        conn.query('UPDATE tipo_vehiculos SET baja = 1 WHERE id = ?', [id], (err, rows) => {
            if(!err){
                res.json({status: 'Tipo de vehiculo eliminado'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.update = (req, res) => {
    const {id} = req.params
    const nuevoTipo = req.body
    req.getConnection( (req, conn) => {
        conn.query('UPDATE tipo_vehiculos SET ? WHERE id = ?', [nuevoTipo, id], (err, rows) => {
            if(!err){
                res.json({status: 'Tipo de vehiculo actualizado'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.edit = (req, res) => {
    const { id } = req.params
    req.getConnection( (req, conn) => {
        conn.query('SELECT * FROM tipo_vehiculos WHERE id = ?', [id], (err, tipovehiculo) => {
            // res.render('customer_edit', { //renderizo la vista de edición y le paso a data el customer
            //     data : customer[0]
            // })
            if(!err){
                res.json(tipovehiculo[0])
            } else {
                console.log(err)
            }
        })
    })
}

module.exports = controller