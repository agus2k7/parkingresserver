const controller = {}
const fetch = require("node-fetch")
const urlapi = 'http://localhost:3000/api'

controller.list = (req, res) => {
    // res.send('hola mundo')
    fetch(urlapi+'/tipovehiculos')
        .then(res => res.json())
        .then(tipoVehiculos => {
            // console.log(tipoVehiculos)
            res.render('tipo_vehiculos', {
                    data: tipoVehiculos})
        })
}

controller.save = (req, res) => {
    console.log(req.body)
    const data = req.body
    fetch(urlapi+'/tipovehiculos', {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data), // data can be `string` or {object}!
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
      .then(message => {
        //   res.render('tipo_vehiculos', {
        //       data: message
        //   })
        res.redirect('tipovehiculos')
      })
}

module.exports = controller