const controller = {}

controller.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM marcas WHERE baja = 0', (err, marcas) => {
            if (!err){
                res.json(marcas)
            } else {
                console.log(err)
            }
        })
    })
}

controller.save = (req, res) => {
    const data = req.body
    console.log(data)
    req.getConnection((err, conn) => {
        conn.query('INSERT INTO marcas SET ?', [data], (err, marcas) => {
            if(!err){
                res.json({status: 'Marca guardada'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.delete = (req, res) => {
    console.log(req.params) //parametros de url
    // const id = req.params.id
    const { id } = req.params //otra forma mas corta de escribir lo mismo
    req.getConnection((err,conn) => {
        conn.query('UPDATE marcas SET baja = 1 WHERE id = ?', [id], (err, rows) => {
            if(!err){
                res.json({status: 'Marca eliminada'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.update = (req, res) => {
    const {id} = req.params
    const nuevaMarca = req.body
    req.getConnection( (req, conn) => {
        conn.query('UPDATE marcas SET ? WHERE id = ?', [nuevaMarca, id], (err, rows) => {
            if(!err){
                res.json({status: 'Marca actualizada'})
            } else {
                console.log(err)
            }
        })
    })
}

controller.edit = (req, res) => {
    const { id } = req.params
    req.getConnection( (req, conn) => {
        conn.query('SELECT * FROM marcas WHERE id = ?', [id], (err, marcas) => {
            if(!err){
                res.json(marcas[0])
            } else {
                console.log(err)
            }
        })
    })
}

module.exports = controller