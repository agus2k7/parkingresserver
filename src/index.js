const express = require('express')
const path = require('path') //modulo para unir directorios
const morgan = require('morgan')//para obtener info de las peticiones que llegan
const mysql = require('mysql')
const myConnection = require('express-myconnection')
const cors = require('cors')

const app = express()

//importando rutas y las llevo a una constante
// const customerRoutes = require('./routes/customer')
const tipoVehiculosRoutes = require ('./routes/tipo_vehiculos')
const tarifasRoutes = require ('./routes/tarifas')
const turnosRoutes = require ('./routes/turnos')
const marcasRoutes = require ('./routes/marcas')
const modelosRoutes = require ('./routes/modelos')
const estadiasRoutes = require ('./routes/estadias')
const clientesRoutes = require ('./routes/clientes')



//*** SETTINGS (puerto, motor de plantillas, carpeta de vistas) ***
app.set('port', process.env.PORT || 3000)//revisa si el SO no asiga un puerto, sino sucede eso usa el 3000 
// app.set('json spaces', 2) //para ver mejor los json
app.set('view engine', 'ejs')// motor de plantillas y tambien
app.set('views', path.join(__dirname, 'views'))//indico donde estan las carpetas de las vistas y
//las uno a través del modulo path con el metodo join,
//__dirname nos da la ruta de app.js segun el SO y la concateno con views
//Esto ya se va a ejecutar en Windows y Linux sin problemas de compatibilidad


//*** MIDDLEWARE (Sirve para alterar o convertir datos antes de que lleguen al servidor) ***
app.use(morgan('dev')) //para ver la info del servidor con morgan en la consola, dev es por defecto para
//ver datos sencillos para desarrollo
app.use(myConnection(mysql, {
    host: 'localhost',
    user: 'root',
    password: '',
    port: 3306,
    database: 'parking',
    timezone: 'utc',
    dateStrings: 'date'
}, 'single')) //single es el modo de conexion
//configuro el urlencoded para poder entender el req.body para capturar los campos del form 
app.use(express.urlencoded({extended: false}))

app.use(express.json()) //para entender los json que ingresan


app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});


//*** ROUTES ***
// app.use('/', customerRoutes)
app.use('/', tipoVehiculosRoutes)
app.use('/', tarifasRoutes)
app.use('/', turnosRoutes)
app.use('/', marcasRoutes)
app.use('/', modelosRoutes)
app.use('/', estadiasRoutes)
app.use('/', clientesRoutes)



//*** STATIC FILES ***
app.use(express.static(path.join(__dirname, 'public')))




//Inicio de servidor: En listen me fijo la variable port de los settings definidos mas arriba
//luego con el comando 'npm run dev' corro lo definido en la seccion scripts -> dev del package.json
//alli voy a utilizar nodemon
app.listen(app.get('port'), () => {
    console.log('Server on port 3000')
})
