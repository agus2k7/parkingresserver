const express = require('express')
const router = express.Router() //devuelve un objeto JS al cual le puedo agregar rutas para luego reutilizarlas

const tarifasApiController = require('../controllers/tarifasApiController')

//** API **/
router.get('/api/tarifas', tarifasApiController.list)
router.post('/api/tarifas', tarifasApiController.save)
router.delete('/api/tarifas/:id', tarifasApiController.delete)
router.put('/api/tarifas/:id', tarifasApiController.update)
router.get('/api/tarifas/:id', tarifasApiController.edit)

//al final exporto el modulo para poder utilzarlo en otro lado (en este caso app.js)
module.exports = router