const express = require('express')
const router = express.Router() //devuelve un objeto JS al cual le puedo agregar rutas para luego reutilizarlas

const turnosApiController = require('../controllers/turnosApiController')

//** API **/
router.get('/api/turnos', turnosApiController.list)
router.post('/api/turnos', turnosApiController.save)
router.delete('/api/turnos/:id', turnosApiController.delete)
router.put('/api/turnos/:id', turnosApiController.update)
router.get('/api/turnos/:id', turnosApiController.edit)

//al final exporto el modulo para poder utilzarlo en otro lado (en este caso app.js)
module.exports = router