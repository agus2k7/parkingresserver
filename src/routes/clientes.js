const express = require('express')
const router = express.Router() //devuelve un objeto JS al cual le puedo agregar rutas para luego reutilizarlas

const clientesApiController = require('../controllers/clientesApiController')

//** API **/
router.get('/api/clientes', clientesApiController.list)
router.post('/api/clientes', clientesApiController.save)
router.delete('/api/clientes/:id', clientesApiController.delete)
router.put('/api/clientes/:id', clientesApiController.update)
router.get('/api/clientes/:id', clientesApiController.edit)
router.get('/api/clientes/patente/:patente', clientesApiController.patente) //get by patente

//al final exporto el modulo para poder utilzarlo en otro lado (en este caso app.js)
module.exports = router