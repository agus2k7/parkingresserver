const express = require('express')
const router = express.Router() //devuelve un objeto JS al cual le puedo agregar rutas para luego reutilizarlas

//voy a escribir toda las urls que voy a usar
// router.get('/', (req,res) => {
//     res.send('hello world')
// })

const tipoVehiculosApiController = require('../controllers/tipoVehiculosApiController')
const tipoVehiculosController = require('../controllers/tipoVehiculosController')

//** API **/
router.get('/api/tipovehiculos', tipoVehiculosApiController.list)
router.post('/api/tipovehiculos', tipoVehiculosApiController.save)
router.delete('/api/tipovehiculos/:id', tipoVehiculosApiController.delete)
router.put('/api/tipovehiculos/:id', tipoVehiculosApiController.update)
router.get('/api/tipovehiculos/:id', tipoVehiculosApiController.edit)

//** VISTAS **/
router.get('/tipovehiculos', tipoVehiculosController.list)
router.post('/tipovehiculos', tipoVehiculosController.save)

//al final exporto el modulo para poder utilzarlo en otro lado (en este caso app.js)
module.exports = router