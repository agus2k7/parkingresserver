const express = require('express')
const router = express.Router() //devuelve un objeto JS al cual le puedo agregar rutas para luego reutilizarlas

//voy a escribir toda las urls que voy a usar
// router.get('/', (req,res) => {
//     res.send('hello world')
// })

//voy a importar el costumeController
const customerController = require('../controllers/customerController')

//rutas con controladores y metodos
router.get('/', customerController.list)
router.post('/add', customerController.save)
router.get('/delete/:id', customerController.delete) //paramatro id
router.get('/update/:id', customerController.edit)
router.post('/update/:id', customerController.update)


//al final exporto el modulo para poder utilzarlo en otro lado (en este caso app.js)
module.exports = router