const express = require('express')
const router = express.Router() //devuelve un objeto JS al cual le puedo agregar rutas para luego reutilizarlas

const marcasApiController = require('../controllers/marcasApiController')

//** API **/
router.get('/api/marcas', marcasApiController.list)
router.post('/api/marcas', marcasApiController.save)
router.delete('/api/marcas/:id', marcasApiController.delete)
router.put('/api/marcas/:id', marcasApiController.update)
router.get('/api/marcas/:id', marcasApiController.edit)

//al final exporto el modulo para poder utilzarlo en otro lado (en este caso app.js)
module.exports = router