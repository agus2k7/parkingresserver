const express = require('express')
const router = express.Router() //devuelve un objeto JS al cual le puedo agregar rutas para luego reutilizarlas

const modelosApiController = require('../controllers/modelosApiController')

//** API **/
router.get('/api/modelos', modelosApiController.list)
router.post('/api/modelos', modelosApiController.save)
router.delete('/api/modelos/:id', modelosApiController.delete)
router.put('/api/modelos/:id', modelosApiController.update)
router.get('/api/modelos/:id', modelosApiController.edit)

//al final exporto el modulo para poder utilzarlo en otro lado (en este caso app.js)
module.exports = router