const express = require('express')
const router = express.Router() //devuelve un objeto JS al cual le puedo agregar rutas para luego reutilizarlas

const estadiasApiController = require('../controllers/estadiasApiController')

//** API **/
router.get('/api/estadias', estadiasApiController.list)
router.post('/api/estadias', estadiasApiController.save)
router.delete('/api/estadias/:id', estadiasApiController.delete)
router.put('/api/estadias/:id', estadiasApiController.update)
router.get('/api/estadias/:id', estadiasApiController.edit)

//al final exporto el modulo para poder utilzarlo en otro lado (en este caso app.js)
module.exports = router